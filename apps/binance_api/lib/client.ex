defmodule BinanceApi.Client do
  @moduledoc false

  defmacro __using__(_) do
    quote do
      import BinanceApi.Client
    end
  end

  use Tesla

  plug(Tesla.Middleware.BaseUrl, Application.get_env(:binance_api, :base_url))

  plug(Tesla.Middleware.Headers, %{
    "X-MBX-APIKEY" => Application.get_env(:binance_api, :api_key)
  })

  plug(Tesla.Middleware.JSON)

  @endpoints %{
    ping: "/v1/ping",
    server_time: "/v1/time",
    exchange_info: "/v1/exchangeInfo",
    all_orders: "/v3/allOrders",
    depth: "/v1/depth",
    trades: "/v1/trades",
    historical_trades: "/v1/historicalTrades",
    aggregate_trades: "/v1/aggTrades",
    candlesticks: "/v1/klines",
    ticker_24hr: "/v1/ticker/24hr",
    ticker_price: "/v3/ticker/price",
    ticker_book: "/v3/ticker/bookTicker",
    new_order: "/v3/order",
    test_new_order: "/v3/order/test"
  }

  def endpoint(key), do: @endpoints[key]

  def timestamp, do: System.system_time(:millisecond)

  defp signature(query_string) do
    secret = Application.get_env(:binance_api, :secret_key)

    :crypto.hmac(:sha256, secret, query_string)
    |> Base.encode16()
  end

  def do_get(url) do
    response = url |> endpoint |> get

    {response.status, response.body}
  end

  def do_get(queryParams, url) do
    response = url |> endpoint |> get(query: queryParams)

    {response.status, response.body}
  end

  def do_get_with_signature(queryParams, url) do
    query = URI.encode_query(queryParams)
    sig = signature(query)
    queryParams = queryParams ++ [signature: sig]

    do_get(queryParams, url)
  end

  def do_post(data, url) do
    body = Enum.into(data, %{})
    body = Poison.encode(body)
    IO.inspect(body)
    response = url |> endpoint |> post(body)

    {response.status, response.body}
  end

  def do_post_with_signature(data, url) do
    query = URI.encode_query(data)
    sig = signature(query)
    data = data ++ [signature: sig]
    # data = Map.put(data, :signature, sig)
    do_post(data, url)
  end
end
