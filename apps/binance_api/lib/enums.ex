defmodule BinanceApi.Enums do
  @moduledoc "Enums for Binance API"

  # Symbol status
  @pre_trading "PRE_TRADING"
  @trading "TRADING"
  @post_trading "POST_TRADING"
  @end_of_day "END_OF_DAY"
  @halt "HALT"
  @auction_match "AUCTION_MATCH"
  @break "BREAK"

  # Symbol type
  @spot "SPOT"

  # Order status
  @new "NEW"
  @partially_filled "PARTIALLY_FILLED"
  @filled "FILLED"
  @canceled "CANCELED"
  @pending_cancel "PENDING_CANCEL"
  @rejected "REJECTED"
  @expired "EXPIRED"

  # Order types
  @limit "LIMIT"
  @market "MARKET"
  @stop_loss "STOP_LOSS"
  @stop_loss_limit "STOP_LOSS_LIMIT"
  @take_profit "TAKE_PROFIT"
  @take_profit_limit "TAKE_PROFIT_LIMIT"
  @limit_maker "LIMIT_MAKER"

  # Order side
  @buy "BUY"
  @sell "SELL"

  # Time in force
  @gtc "GTC"
  @ioc "IOC"
  @fok "FOK"

  # Kline/Candlestick chart intervals
  # m -> minutes; h -> hours; d -> days; w -> weeks; M -> months
  @_1m "1m"
  @_3m "3m"
  @_5m "5m"
  @_15m "15m"
  @_30m "30m"
  @_1h "1h"
  @_2h "2h"
  @_4h "4h"
  @_6h "6h"
  @_8h "8h"
  @_12h "12h"
  @_1d "1d"
  @_3d "3d"
  @_1w "1w"
  @_1M "1M"

  # Rate limiters (rateLimitType)
  @requests "REQUESTS"
  @orders "ORDERS"

  # Rate limit intervals
  @second "SECOND"
  @minute "MINUTE"
  @day "DAY"

  def symbol_status,
    do: %{
      pre_trading: @pre_trading,
      trading: @trading,
      post_trading: @post_trading,
      end_of_day: @end_of_day,
      halt: @halt,
      auction_match: @auction_match,
      break: @break
    }

  def symbol_type,
    do: %{
      spot: @spot
    }

  def order_status,
    do: %{
      new: @new,
      partially_filled: @partially_filled,
      filled: @filled,
      canceled: @canceled,
      pending_cancel: @pending_cancel,
      rejected: @rejected,
      expired: @expired
    }

  def order_types,
    do: %{
      limit: @limit,
      market: @market,
      stop_loss: @stop_loss,
      stop_loss_limit: @stop_loss_limit,
      take_profit: @take_profit,
      take_profit_limit: @take_profit_limit,
      limit_maker: @limit_maker
    }

  def order_side,
    do: %{
      buy: @buy,
      sell: @sell
    }

  def time_in_force,
    do: %{
      gtc: @gtc,
      ioc: @ioc,
      fok: @fok
    }

  def chart_intervals,
    do: %{
      _1m: @_1m,
      _3m: @_3m,
      _5m: @_5m,
      _15m: @_15m,
      _30m: @_30m,
      _1h: @_1h,
      _2h: @_2h,
      _4h: @_4h,
      _6h: @_6h,
      _8h: @_8h,
      _12h: @_12h,
      _1d: @_1d,
      _3d: @_3d,
      _1w: @_1w,
      _1M: @_1M
    }

  def rate_limiters,
    do: %{
      requests: @requests,
      orders: @orders
    }

  def rate_limit_intervals,
    do: %{
      second: @second,
      minute: @minute,
      day: @day
    }
end
