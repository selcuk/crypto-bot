defmodule BinanceApi do
  @moduledoc """
  Documentation for BinanceApi.
  """

  use BinanceApi.Client

  def filter_nil(query) do
    Enum.filter(query, fn {_, v} -> v != nil end)
  end

  def ping do
    do_get(:ping)
  end

  def get_server_time do
    do_get(:server_time)
  end

  def get_exchange_info do
    do_get(:exchange_info)
  end

  def get_all_orders(symbol) do
    [symbol: symbol, timestamp: timestamp()]
    |> do_get_with_signature(:all_orders)
  end

  def get_depth(symbol, limit \\ nil) do
    [symbol: symbol, limit: limit] |> filter_nil |> do_get(:depth)
  end

  def get_trades(symbol, limit \\ nil) do
    [symbol: symbol, limit: limit]
    |> filter_nil
    |> do_get(:trades)
  end

  def get_historical_trades(symbol, limit \\ nil, trade_id \\ nil) do
    [symbol: symbol, limit: limit, fromId: trade_id]
    |> filter_nil
    |> do_get(:historical_trades)
  end

  def get_aggregate_trades(
        symbol,
        limit \\ nil,
        start_time \\ nil,
        end_time \\ nil,
        trade_id \\ nil
      ) do
    [symbol: symbol, limit: limit, startTime: start_time, endTime: end_time, fromId: trade_id]
    |> filter_nil
    |> do_get(:aggregate_trades)
  end

  def get_klines(symbol, interval, limit \\ nil, start_time \\ nil, end_time \\ nil) do
    [symbol: symbol, interval: interval, limit: limit, startTime: start_time, endTime: end_time]
    |> filter_nil
    |> do_get(:candlesticks)
  end

  def get_ticker_24hr(symbol \\ nil) do
    [symbol: symbol]
    |> filter_nil
    |> do_get(:ticker_24hr)
  end

  def get_ticker_price(symbol \\ nil) do
    [symbol: symbol]
    |> filter_nil
    |> do_get(:ticker_price)
  end

  def get_ticker_book(symbol \\ nil) do
    [symbol: symbol]
    |> filter_nil
    |> do_get(:ticker_book)
  end

  def post_new_order(args) do
    # [
    #   symbol: symbol,
    #   side: side,
    #   type: type,
    #   quantity: quantity,
    #   timestamp: timestamp,
    #   timeInForce: timeInForce,
    #   price: price,
    #   newClientOrderId: newClientOrderId,
    #   stopPrice: stopPrice,
    #   icebergQty: icebergQty,
    #   newOrderRespType: newOrderRespType,
    #   recvWindow: recvWindow
    # ]
    # BinanceApi.Request.NewOrder.to_keyword_list(args)
    args
    |> filter_nil
    |> do_post_with_signature(:test_new_order)
  end
end
