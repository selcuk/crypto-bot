defmodule BinanceApi.Request.NewOrder do
  @enforce_keys [:symbol, :side, :type, :quantity, :timestamp]
  defstruct [
    :symbol,
    :side,
    :type,
    :quantity,
    :timestamp,
    :timeInForce,
    :price,
    :newClientOrderId,
    :stopPrice,
    :icebergQty,
    :newOrderRespType,
    :recvWindow
  ]

  def to_keyword_list(dict) do
    Enum.map(dict, fn {key, value} -> {key, value} end)
  end
end
