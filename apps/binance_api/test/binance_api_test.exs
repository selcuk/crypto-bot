defmodule BinanceApiTest do
  use ExUnit.Case
  doctest BinanceApi

  test "test connectivity" do
    assert BinanceApi.ping() == {200, %{}}
  end

  test "check server time" do
    {status, server_time} = BinanceApi.get_server_time()
    assert status == 200
    assert %{"serverTime" => time} = server_time
    assert is_integer(time)
  end

  test "exchange information" do
    {status, response} = BinanceApi.get_exchange_info()
    assert status == 200

    assert %{
             "timezone" => "UTC",
             "serverTime" => server_time,
             "rateLimits" => rate_limits,
             "exchangeFilters" => exchange_filters,
             "symbols" => symbols
           } = response

    assert is_integer(server_time)
    assert is_list(rate_limits)
    assert is_list(exchange_filters)
    assert is_list(symbols)

    rate_limiter_values = Map.values(BinanceApi.Enums.rate_limiters())
    rate_limit_intervals = Map.values(BinanceApi.Enums.rate_limit_intervals())

    is_rate_limits_valid =
      Enum.all?(rate_limits, fn rate_limit ->
        validate_rate_limit(rate_limit, rate_limiter_values, rate_limit_intervals)
      end)

    is_symbols_valid = Enum.all?(symbols, fn symbol -> validate_symbol(symbol) end)

    assert is_rate_limits_valid && is_symbols_valid
  end

  defp validate_rate_limit(
         %{"rateLimitType" => rate_limit_type, "interval" => interval, "limit" => limit},
         rate_limiter_values,
         rate_limit_intervals
       ) do
    is_limit_type_valid = Enum.any?(rate_limiter_values, fn n -> rate_limit_type == n end)
    is_interval_valid = Enum.any?(rate_limit_intervals, fn n -> interval == n end)
    is_limit_valid = is_integer(limit)

    is_limit_type_valid && is_interval_valid && is_limit_valid
  end

  defp validate_symbol(%{
         "symbol" => _,
         "status" => status,
         "baseAsset" => _,
         "baseAssetPrecision" => _,
         "quoteAsset" => _,
         "quotePrecision" => _,
         "orderTypes" => _,
         "icebergAllowed" => _,
         "filters" => _
       }) do
    symbol_status = Map.values(BinanceApi.Enums.symbol_status())

    Enum.any?(symbol_status, fn n -> status == n end)
  end
end
